<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class Articles extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $article = new Article();
        $article->setTitre('9 Millions');
        $article->setDescription('Neuf millions d’euros, c’est le butin dérobé vendredi 28 août 2020 dans l’attaque à Lyon d’un fourgon de transport de fonds de la Société Loomis rue Abraham Bloch dans le 7e arrondissement. Le fourgon venait de sortir de la Banque de France quand il a été pris en étau par deux fourgons d’où est sortie une équipe de 4 braqueurs lourdement armés et déterminés. ');
        $article->setImage('9M.jpg');

        $manager->persist($article);
        $manager->flush();

        $article = new Article();
        $article->setTitre('Covid-19');
        $article->setDescription('Les expériences qui utilisent des chiens entraînés à détecter par l\'odorat les personnes contaminées par le nouveau coronavirus donnent des «résultats prometteurs», mais il faut en «compléter l\'évaluation scientifique» et «définir des règles de bon usage», estiment lundi les Académies vétérinaire et de médecine.');
        $article->setImage('coco.jpeg');

        $manager->persist($article);
        $manager->flush();
    }
}
