<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class Users extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->getUsername('test');
        $user->getPassword('test');
        $user->getRoles('journaliste');

        $manager->persist($user);
        $manager->flush();
    }
}
