<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index()
    {
        $articles = $this->getDoctrine()->getRepository(Article::class)->findAll();

        return $this->render('default/index.html.twig', [
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/detail/{article}", name="detail_article")
     */

    public function detailArticle(Article $article)
    {
        return $this->render('article/detail.html.twig', ['article' => $article]);
    }

    /**
     * @Route("/article/{article}/delete", name="article_delete")
     */
    public function delete(Article $article)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($article);
        $entityManager->flush();
    }
}
