<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article", name="article")
     */
    public function index()
    {
        return $this->render('article/index.html.twig', [
            'controller_name' => 'ArticleController',
        ]);
    }


    /**
     * @Route("/add", name="add")
     */
    public function addArticle(Request $request){
        $form = $this->createForm(ArticleType::class, new Article());

        $form->handleRequest($request);

        if($form->isSubmitted() and $form->isValid()){
            $article = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();
            return $this->redirectToRoute('default');
        } else {
            return $this->render('article/add.html.twig',
                [
                    'form'=> $form->createView(),
                    'errors'=> $form->getErrors()
                ]);
               }
    }
}
